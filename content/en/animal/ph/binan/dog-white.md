---
title: "White Dog"
subtitle: ""
# date: 2021-05-24
form: "white dog Angels Shelter"
image: "/pics/mh/sanpedro/dogw.jpg"
pics: ["/pics/mh/sanpedro/dogw.jpg","/pics/mh/sanpedro/dogw2.jpg","/pics/mh/sanpedro/dogw3.jpg"]
linkb: /animal/ph/sanpedro/dog-black
linkbtext: "Black Dog"
linkf: /animal/ph/sanpedro/
linkftext: "Pets"
# chart: "<script> const labels = ['Jan', 'Feb', 'Mar', 'Apr', 'May']; const data = {   labels: labels,   datasets: [{     label: 'Value of Collections in points',     data: [65, 59, 80, 81, 56],     fill: false,     borderColor: 'rgb(75, 192, 192)',     tension: 0.1   }] }; const config = {     type: 'line',     data: data,   }; const Chart1 = new Chart(     document.getElementById('Chart1'),     config   );   const data2 = {   labels: labels,   datasets: [{     label: 'Number of Beneficiaries',     data: [5, 9, 30, 8, 61],     fill: false,     borderColor: 'rgb(175, 92, 9)',     tension: 0.1   }] }; const config2 = {     type: 'line',     data: data2,   }; const Chart2 = new Chart(     document.getElementById('Chart2'),     config2   );  const data3 = {   labels: labels,   datasets: [{     label: 'Number of Food Givers',     data: [25, 9, 10, 8, 6],     fill: false,     borderColor: 'rgb(7, 92, 92)',     tension: 0.1   }] }; const config3 = {     type: 'line',     data: data3,   }; const Chart3 = new Chart(     document.getElementById('Chart3'),     config3   ); const data4 = {   labels: labels,   datasets: [{     label: 'Money Donations Received',     data: [59, 99, 90, 38, 26],     fill: false,     borderColor: 'rgb(87, 2, 192)',     tension: 0.1   }] };	 const config4 = {     type: 'line',     data: data4,   }; const Chart4 = new Chart(     document.getElementById('Chart4'),     config4   ); const data5 = {   labels: labels,   datasets: [{     label: 'Kilos Wasted',     data: [19, 59, 9, 38, 2],     fill: false,     borderColor: 'rgb(7, 2, 92)',     tension: 0.1   }] };	 const config5 = {     type: 'line',     data: data5,   }; const Chart5 = new Chart(     document.getElementById('Chart5'),     config5   );</script>"

---

Age | Weight | Issues
--- | --- | ---
1 year | 1 kg | Skin Disease

Please donate to the Gcash below or by sending a message:

![Donate](/ui/astergcash.jpg)

