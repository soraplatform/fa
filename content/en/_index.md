---
org: "Food for All"
image: "/ui/foodserve.jpg"

####################### Banner #########################
banner:
  bannermain: "/ui/foodserve.jpg"
  title : "Food for All"
  # Let's Build a Better World After the Great Reset
  # image : "banner-art.svg"
  youtube: "HKGRgEoORiU"
  content : "We are a movement to remove involuntary hunger and malnutrition from the world"
  mainbuttons:
    - label : "Register in the Waitlist"
      color: "primary"
      link : "https://hub.pantrypoints.com/signup"
    - label : "Check out our system"
      color: "warning"
      link : "https://pantrypoints.com"


################### Plat Skills ########################

showcase:
  title : "The Zero Waste Circular Economy"
  subtitle: "We use technology to implement the circular economy to granularly monitor food supply and measure the accountability and impact of our feeding programs"
  item:
    #- name : "Pantry"
    #  image : "icons/pantry.png"
    #  content : "A waste to wealth system that uses points and point-banks instead of currency, to de-commercialize an economy and bank the unbanked"
    #  link: "pantry"

    - name : Food Rescue
      image : /ui/foodbox.jpg
      content : "We collect food waste from markets and retailers"

    # - name : Food Rescue
    #   image : /ui/code.jpg
    #   content : "Donors can donate online and monitor the status of each feeding program"
      # link: home

    - name : Feeding Programs
      image : /pics/1small.jpg
      content : "We use the collected waste for freeding programs"
      # link: banking

    - name : Analytics
      image : /ui/ai.jpg
      content : We will use real-time analytics to maximize the utilization of volunteers
      # link: world


locations:
  title : "Programs"
  subtitle: "Check out our locations"
  sitem: 
    - name: "Food Rescue"
      img: /logos/camsur.jpg    
      link: "/food/ph/camsur"
      flag: "/flags/ph.png"
      btn: "is-primary"      
    - name: "Animal Rescue"
      img: /logos/sanp.jpg    
      link: "/animal/ph/sanpedro"
      flag: "/flags/ph.png"
      btn: "is-primary"
    - name: "Donation Drive"
      img: /logos/chiang.jpg
      link: "/donation/th/chiang-mai"
      flag: "/flags/th.png"
      btn: "is-primary"
    # - name: "Saigon"
    #   link: ""
    #   flag: "https://sorasystem.sirv.com/flags/vn.svg"
    #   btn: "is-danger"
    # - name: "Add your country!"
    #   link: "https://world.pantrypoints.com"
    #   flag: "/icons/plus.png"
    #   btn: "is-primary"




##################### Feature ##########################
feature:
  bannerfeat: "/ui/happy.jpg"
  title : "Real Impact With Every Donation"
  image: ""
  feature_item:
    - name : "Maximized Impact, Minimized Costs"
      icon : "<svg xmlns='http://www.w3.org/2000/svg' fill='crimson' class='icon is-large' viewBox='0 0 640 512'><!-- Font Awesome Free 5.15.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License) --><path d='M621.16 54.46C582.37 38.19 543.55 32 504.75 32c-123.17-.01-246.33 62.34-369.5 62.34-30.89 0-61.76-3.92-92.65-13.72-3.47-1.1-6.95-1.62-10.35-1.62C15.04 79 0 92.32 0 110.81v317.26c0 12.63 7.23 24.6 18.84 29.46C57.63 473.81 96.45 480 135.25 480c123.17 0 246.34-62.35 369.51-62.35 30.89 0 61.76 3.92 92.65 13.72 3.47 1.1 6.95 1.62 10.35 1.62 17.21 0 32.25-13.32 32.25-31.81V83.93c-.01-12.64-7.24-24.6-18.85-29.47zM48 132.22c20.12 5.04 41.12 7.57 62.72 8.93C104.84 170.54 79 192.69 48 192.69v-60.47zm0 285v-47.78c34.37 0 62.18 27.27 63.71 61.4-22.53-1.81-43.59-6.31-63.71-13.62zM320 352c-44.19 0-80-42.99-80-96 0-53.02 35.82-96 80-96s80 42.98 80 96c0 53.03-35.83 96-80 96zm272 27.78c-17.52-4.39-35.71-6.85-54.32-8.44 5.87-26.08 27.5-45.88 54.32-49.28v57.72zm0-236.11c-30.89-3.91-54.86-29.7-55.81-61.55 19.54 2.17 38.09 6.23 55.81 12.66v48.89z'/></svg>"
      content : "We tap local volunteers who benefit directly from the programs"
      
    - name : "Meant for crises"
      icon : "<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 320 512' fill='crimson' class='icon is-large'><!-- Font Awesome Free 5.15.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License) --><path d='M296 160H180.6l42.6-129.8C227.2 15 215.7 0 200 0H56C44 0 33.8 8.9 32.2 20.8l-32 240C-1.7 275.2 9.5 288 24 288h118.7L96.6 482.5c-3.6 15.2 8 29.5 23.3 29.5 8.4 0 16.4-4.4 20.8-12l176-304c9.3-15.9-2.2-36-20.7-36z'/></svg>"
      content : "Our system addresses resource allocation in whatever situation"
    
    - name : "Networked with other NGOs"
      icon : "<svg xmlns='http://www.w3.org/2000/svg' fill='crimson' class='icon is-large' viewBox='0 0 640 512'><!-- Font Awesome Free 5.15.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License) --><path d='M488 192H336v56c0 39.7-32.3 72-72 72s-72-32.3-72-72V126.4l-64.9 39C107.8 176.9 96 197.8 96 220.2v47.3l-80 46.2C.7 322.5-4.6 342.1 4.3 357.4l80 138.6c8.8 15.3 28.4 20.5 43.7 11.7L231.4 448H368c35.3 0 64-28.7 64-64h16c17.7 0 32-14.3 32-32v-64h8c13.3 0 24-10.7 24-24v-48c0-13.3-10.7-24-24-24zm147.7-37.4L555.7 16C546.9.7 527.3-4.5 512 4.3L408.6 64H306.4c-12 0-23.7 3.4-33.9 9.7L239 94.6c-9.4 5.8-15 16.1-15 27.1V248c0 22.1 17.9 40 40 40s40-17.9 40-40v-88h184c30.9 0 56 25.1 56 56v28.5l80-46.2c15.3-8.9 20.5-28.4 11.7-43.7z'/></svg>"
      content : "We have links with other charities and NGOs that do similar things that we do"
    


######################### How it works #####################
how:
  title : "How it Works"
  steps:
    - id: 1
      content: "Choose a pogram you want to support"
      image: "/pics/1small.jpg"    
    - id: 2
      content: "Post the details of your donation"
      image: "/ui/donor.png"
    - id: 3
      content: "Earn social points and check the results online"
      image: "/ui/bed.jpg"      
        


partners:
  title: "Partners"
  item:
    - # ame : "Animal Shelter"
      image: "/logos/shelter200.png"
      link: "https://animalshelter.shop"      
    - # name : "Neo Holistic Shop"
      image: "/logos/neo.png"
      link: "https://neoholistic.shop"
      # link: "http://adamsmithslostlegacy.blogspot.com"
    - # name : "Pantry Points Shop"
      image: "/logos/shop.png"
      link: "https://pantrypoints.com/front"


feedback:
  title: "Feedback"
  item:
    - user : "Hung Pham, Donor"
      image: "/avatars/hung.jpg"
      content: "The work is very inspiring and timely especially with the globa inflation and uncertainty"
      # link: "http://www.dhanjooghista.com"
    - user : "Huong Nguyen, research student"
      image: "/avatars/huong.jpg"
      content: "Food for all has a great potential to eliminate hunger and malnutrition" 
      # link: "http://adamsmithslostlegacy.blogspot.com"


##################### Call to action #####################
# call_to_action:
#   title : "Let's create the new system!"
#   link1text: "I'm in!"
#   link1 : "https://hub.pantrypoints.com/signup"
#   link2text: "I'm not convinced yet"
#   link2 : "https://superphysics.one/pantrynomics/"
  
---
