---
title: "Construction of Sanitation Facilities"
subtitle: ""
date: 2021-07-04
form: "test"
image: "/pics/th/fathai1s.jpg"
pics: ["/pics/th/fathai1s.jpg", "/pics/th/fathai2s.jpg", "/pics/th/fathai3s.jpg", "/pics/th/fathai4s.jpg"]
---

We donated materials for the construction of toilets in rural Chiang Mai, Thailand.
 