---
title: "Public School Feeding Program"
subtitle: "Barangay Concepcion Bicol"
date: 2021-07-04
form: "Camsur Barangay Concepcion"
image: "/pics/mh/bicol/fabicol1s.jpg"
pics: ["/pics/mh/bicol/fabicol1s.jpg", "/pics/mh/bicol/fabicol2s.jpg", "/pics/mh/bicol/fabicol3s.jpg", "/pics/mh/bicol/fabicol4s.jpg", "/pics/mh/bicol/fabicol5s.jpg"]
linkb: /food/ph/21-07-02
linkbtext: "July 2"
linkf: /food/ph/21-07-11
linkftext: "July 11"
---

It’s good to end the day with positive words thought and deeds no matter how hard things were during this pandemic.

Today we extended our Service according to our capacity. The kids were happy to get their hot meals. 

<!-- When observing   the kids before and after they get their Hot Meals
  With a smile and happy behavior. -->

It will be good if we get humanitarian assistance so that we can prevent the increasing number of malnutrition in the village and the province as a whole.

No doubt, food is very essential to our body and mind but also it's just unaffordable for the few. Poor families face a daily struggle to survive during these hard times.

Tour drop of Blessing can be life-saving for many.
<!-- Yogi
Yogi Parivrata
 -->