---
title: "I want to help!"
subtitle: "Tell us how you want to help"
image: "/flite.png"
# meta description
description: "Help Food for All by donating"
---

We are in need of:

- food donations 
- monetary donations
- volunteers

Leave us a message or send an email:

* **Mail: hello@foodforall.xyz**

<!-- * **Phone: +88 125 256 452** 
* **Address: 360 Main rd, Rio, Brazil** -->
