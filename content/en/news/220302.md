---
title: "angelshelter.shop is now online!"
subtitle: ""
image: "/logos/shelter.png"
pics: [/logos/shelter.png]
---

We put up angelshelter.shop to list our animals for adoption, as well as to provide an easy way for interested people to donate directly.
