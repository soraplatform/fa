---
title: "Count me in!"
subtitle: ""
# meta description
description: "Donate to Food for All!"
draft: false
---

### Leave us a message or a comment
Or send an email:

* **Mail: hello@foodforall.xyz**
