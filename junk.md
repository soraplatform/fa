<!--               <img width="{{site.Params.logo_width}}" style="-webkit-filter: grayscale(100%); filter: grayscale(100%)" src="{{ . | relURL }}" alt="{{ site.Title }}"> -->

data -> config -> chart


<script>
  const labels = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
  ];

  const data = {
    labels: labels,
    datasets: [{
      label: 'Paper waste kilos collected',
      backgroundColor: 'rgb(255, 99, 132)',
      borderColor: 'rgb(255, 99, 132)',
      data: [0, 10, 5, 2, 20, 30, 45],
    }]
  };

  const config = {
    type: 'bar',
    data: data,
    options: {
      responsive: true,
      plugins: {
        legend: {
          position: 'top',
        },
        title: {
          display: true,
          text: 'Chart.js Bar Chart'
        }
      }
    },
  };

  const Chart1 = new Chart(
    document.getElementById('Chart1'),
    config
  );
</script>



| safeURL

          <!-- <div class="column is-6-desktop is-4-mobile">
            <img src="{{ . | safeURL }}" alt="Food for all" style="width:100%;max-width:300px" id="img{{ . }}">
          </div> -->
          

          <div id="{{ . }}" class="modal">
            <!-- The Close Button -->
            <span class="close">&times;</span>
            <!-- Modal Content (The Image) -->
            <img class="modal-content" src="{{ . }}">
            <!-- Modal Caption (Image Text) -->
            <div id="caption"></div>
          </div>
          <script type="text/javascript">
            // Get the modal
            var img = document.getElementById("img{{ . }}");
            // getElementsByClassName
            // Get the image and insert it inside the modal - use its "alt" text as a caption
            var modal = document.getElementById({{ . }});

            console.log(modal);
            // var img = document.getElementsByClassName("clickable")
            // var modalImg = document.getElementsByClassName("clickable");
            // var captionText = document.getElementById("caption");
            // var modalImg = document.getElementById("modal-{{ . }}");
            // var captionText = document.getElementById("caption");

            img.onclick = function(){
              modal.style.display = "block";
            }
            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName("close")[0];
            // When the user clicks on <span> (x), close the modal
            span.onclick = function() {
              modal.style.display = "none";
            } 
            </script>

            

    <div class="columns is-mobile is-multiline">
      {{ $paginator := .Paginate .Data.Pages }}
      
      {{ range $paginator.Pages }}
        <div class="column is-4-desktop is-12-mobile">
          <a href="{{ .Permalink }}" title="{{ .Title }}" class="">
            <div class="card card-equal-height">
              <div class="card-image">
                <figure class="image is-16by9">
                  <img src="{{ .Params.image | relURL }}" alt="{{ .Title }}" style="display: flex !important;">                
                </figure>
              </div>
              <div class="card-content">
                <div class="content">         
                  <h3 class="title is-4 mt-4">
                    {{ .Title }}
                  </h3>
                  {{ with .Params.date }}
                    <small>{{ .Format "January 2, 2006" }}</small>
                  {{ end }}
                  <p class="card-content">{{ .Summary }}</p>
                </div>
              </div>
            </div>
          </a>
        </div>
      {{ end }}
    </div>

    {{ template "_internal/pagination.html" . }}

    
    <div class="has-text-centered pb-4">
      <h1 class="title is-1">{{ .Params.title }}</h1>
      <h2 class="subtitle is-3">{{ .Params.subtitle }}</h2>  
    </div>


          <div class="my-4">
            <a href="{{ .Params.applink }}" class="button is-rounded is-primary is-fullwidth">
              {{ .Params.apptext }} <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="icon pl-2" fill="white"><!-- Font Awesome Free 5.15.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License) --><path d="M190.5 66.9l22.2-22.2c9.4-9.4 24.6-9.4 33.9 0L441 239c9.4 9.4 9.4 24.6 0 33.9L246.6 467.3c-9.4 9.4-24.6 9.4-33.9 0l-22.2-22.2c-9.5-9.5-9.3-25 .4-34.3L311.4 296H24c-13.3 0-24-10.7-24-24v-32c0-13.3 10.7-24 24-24h287.4L190.9 101.2c-9.8-9.3-10-24.8-.4-34.3z"/></svg>
            </a>
          </div>        



  <!-- feature -->
  {{ with .Params.feature }}
    {{ partial "feature.html" . }}
  {{ end }}
  <!-- /feature -->


  {{ with .Params.platform }}
    {{ partial "platform.html" . }}
  {{ end }}


  {{ with .Params.how }}
    {{ partial "how.html" . }}
  {{ end }}

  {{ with .Params.segment }}
    {{ partial "segment.html" . }}
  {{ end }}


  {{ with .Params.req }}
    {{ partial "req.html" . }}
  {{ end }}


  {{ with .Params.feedback }}
    {{ partial "feedback.html" . }}
  {{ end }}



  {{ with .Params.call_to_action }}
    {{ partial "cta.html" . }}
  {{ end }}


  <!-- <section class="section">
    <div class="container">
      <div class="row">
        <div class="col-lg-10 mx-auto">
          <div class="content">{{ .Content }}</div>
        </div>
      </div>
    </div>
  </section> -->

{{ end }}
